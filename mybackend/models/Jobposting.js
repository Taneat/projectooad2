const mongoose = require('mongoose')
const jobpostingSchema = new mongoose.Schema({
  namejob: String,
  namecompany: String,
  jobtype: String,
  require: Number,
  responsibility: String,
  requirements: String,
  provinces: String,
  contact: String,
  salary: Number

})

module.exports = mongoose.model('Jobposting', jobpostingSchema) || mongoose.models.Jobposting
