const express = require('express')
const router = express.Router()
const JobpostingController = require('../controller/JobpostingController')

router.get('/', JobpostingController.getUsers)
router.post('/search', JobpostingController.getSearch)
router.get('/:id', JobpostingController.getUser)
router.post('/', JobpostingController.addUser)
router.put('/', JobpostingController.updateUser)
router.delete('/:id', JobpostingController.deleteUser)


module.exports = router
