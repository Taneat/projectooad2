const express = require('express')
const router = express.Router()
const LoginController = require('../controller/LoginController')
// const Login = require('../models/Login')

/* GET users listing. */
router.get('/', LoginController.getUsers)
router.get('/:id', LoginController.getUser)
router.post('/', LoginController.addUser)
router.put('/', LoginController.updateUser)
router.delete('/:id', LoginController.deleteUser)

module.exports = router
